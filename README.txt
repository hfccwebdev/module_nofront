No Front 1.x for Drupal 6.x
---------------------------

I had a discussion a couple of years ago  about how to best create a Drupal
front page that contains no content. That is, we want to build front pages
that don't contain normal Drupal node-based content. Our front pages are
made up of blocks instead.

A favored theming technique is to create a blank page and then add blocks to
contain everything on the front page. My technique is similar, except that
for my last site, I simply omitted rendering $content in my
page-front.tpl.php file, and created a bunch of custom regions that only
appeared there. So what's the problem?

The problem, my friends, is performance.

The empty node method requires a node_load to occur, even though the node
contains no content. My method is even worse, because it still runs the
default frontpage view to generate content it will never render.

This simple module uses hook_menu_alter() to hijack the page callback for
$items['node'] (Drupal's default homepage) and return an empty string
instead of generating any content.

Installation
------------
No Front can be installed like any other Drupal module -- place it in
the modules directory for your site and enable it.
